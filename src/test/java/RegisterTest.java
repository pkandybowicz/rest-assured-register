import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

public class RegisterTest {
    String REGISTER_URL = "http://automationpractice.com/index.php";

    @Test
    void shouldRegisterNewUserWhenCorrectDataIsPassed() throws IOException {

        String responseBody =
                when().get(REGISTER_URL).
                        then().extract().body().asString();

        String token = extractTokenFromBody(responseBody);

        String email = getRandomEmail();
        String password = "23t23g23ffqfaf";

        given().header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8").
                log().all().
                and().formParam("controller", "authentication").
                and().formParam("SubmitCreate", 1).
                and().formParam("ajax", true).
                and().formParam("email_create", email).
                and().formParam("back", "my-account").
                and().formParam("token", token).
                when().post(REGISTER_URL).
                then().statusCode(200);

        given().header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8").
                and().queryParam("controller", "authentication").
                log().all().
                and().formParam("customer_firstname", "dqwwqdqwd").
                and().formParam("customer_lastname", "qgfwqwgqfqgf").
                and().formParam("email", email).
                and().formParam("passwd", password).
                and().formParam("days", 3).
                and().formParam("months", 7).
                and().formParam("years", 1955).
                and().formParam("firstname", "dqwwqdqwd").
                and().formParam("lastname", "qgfwqwgqfqgf").
                and().formParam("company", "safa").
                and().formParam("address1", "wgqgqwgqwgqw").
                and().formParam("address2", "wgeweg2").
                and().formParam("city", "gqwgqwgqwgqw").
                and().formParam("id_state", 14).
                and().formParam("postcode", 12345).
                and().formParam("id_country", 21).
                and().formParam("other", "").
                and().formParam("phone", "").
                and().formParam("phone_mobile", 124512521).
                and().formParam("alias", "My address").
                and().formParam("dni", "").
                and().formParam("email_create", 1).
                and().formParam("is_new_customer", 1).
                and().formParam("back", "my-account").
                and().formParam("submitAccount", "").
                when().post(REGISTER_URL).
                then().statusCode(302);

        System.out.println("Login credentials: " + email + " " + password);

    }

    private String getRandomEmail() {
        Random rnd = new Random();
        return "newEmail" + rnd.nextInt(12943128) + "@wp.pl";
    }

    private String extractTokenFromBody(String responseBody) {
        Pattern pattern = Pattern.compile("var token = '(.*?)'");
        Matcher matcher = pattern.matcher(responseBody);
        if (matcher.find()) {
            return matcher.group(1);
        } else
            return null;
    }

}
